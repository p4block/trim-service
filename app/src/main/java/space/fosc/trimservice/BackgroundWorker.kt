package space.fosc.trimservice

import android.app.IntentService
import android.content.Intent
import com.topjohnwu.superuser.ShellUtils

class BackgroundWorker : IntentService(BackgroundWorker::class.simpleName) {

    override fun onHandleIntent(intent: Intent?) {
        runCommand("fstrim /data")
    }
}