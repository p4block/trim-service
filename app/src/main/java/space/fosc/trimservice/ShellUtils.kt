package space.fosc.trimservice

import com.topjohnwu.superuser.Shell

fun runCommand(command : String) : String {
    return Shell.su(command).exec().out.joinToString("\n")
}
