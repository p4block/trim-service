package space.fosc.trimservice

import com.topjohnwu.superuser.BusyBox
import com.topjohnwu.superuser.ContainerApp
import com.topjohnwu.superuser.Shell

class TRIMService : ContainerApp() {

    override fun onCreate() {
        super.onCreate()

        Shell.Config.setFlags(Shell.FLAG_REDIRECT_STDERR)
        Shell.Config.verboseLogging(BuildConfig.DEBUG)
        BusyBox.setup(this)
    }
}